package com.example.LoginSecurityThymeleaf.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class Department {

	
	

	@Override
	public String toString() {
		return "Department [dept_Id=" + dept_Id + ", deleted=" + deleted + ", dept_Name=" + dept_Name + ", employees="
				+ employees + "]";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long dept_Id;
	private boolean deleted;
	private String dept_Name;
	private String status;
	
	 @OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
	    @JsonIgnore
	    private List<Employee> employees;

	}
