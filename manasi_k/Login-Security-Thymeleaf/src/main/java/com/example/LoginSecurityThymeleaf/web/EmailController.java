package com.example.LoginSecurityThymeleaf.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.LoginSecurityThymeleaf.model.Employee;
import com.example.LoginSecurityThymeleaf.repo.EmployeeRepo;
import com.example.LoginSecurityThymeleaf.service.UserServiceImpl;


@Controller
public class EmailController {

	
	// private static final Logger LOG = LoggerFactory.getLogger(EmailController.class);
	
	@Autowired
	private EmployeeRepo emprepo;
	
	@Autowired
	private UserServiceImpl service;
	
	 @Autowired
	    private JavaMailSender javaMailSender;
	
	@RequestMapping("send-mail")
	public String send(@Validated Employee employee,Model model) {

		
		//model.addAttribute("employee1",	new Employee());
		System.out.println("inside");
		//service.sendEmail(employee);
		System.out.println("send");
		//service.send("abc", "hello");
		
		SimpleMailMessage mail = new SimpleMailMessage();
		
//		String empemail;
//		empemail=emp.getEmail();
		mail.setTo(employee.getEmail());
		mail.setSubject("Testing Mail API");
		mail.setText("Hurray ! You have done that dude...");

		
		javaMailSender.send(mail);
		
		
		
		return "sentemail";
		
	}
	
	 @GetMapping(value = "/email/{user-email}")
	    public @ResponseBody
	    String sendSimpleEmail(@PathVariable("user-email") String email) {
		 
		 
		 service.sendSimpleEmail(email, "Welcome", "This is a welcome email for your!!");
	 
	        return "sentemail";
	    }

	 


	
	
}
