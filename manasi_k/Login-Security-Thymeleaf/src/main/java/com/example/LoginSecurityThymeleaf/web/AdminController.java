package com.example.LoginSecurityThymeleaf.web;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.example.LoginSecurityThymeleaf.model.Department;
import com.example.LoginSecurityThymeleaf.model.Employee;
import com.example.LoginSecurityThymeleaf.model.Months;
import com.example.LoginSecurityThymeleaf.model.RewardType;
import com.example.LoginSecurityThymeleaf.model.Rewards;
import com.example.LoginSecurityThymeleaf.repo.DepartmentRepo;
import com.example.LoginSecurityThymeleaf.repo.EmployeeRepo;
import com.example.LoginSecurityThymeleaf.repo.MonthsRepo;
import com.example.LoginSecurityThymeleaf.repo.RewardRepo;
import com.example.LoginSecurityThymeleaf.repo.RewardTypeRepo;
import com.example.LoginSecurityThymeleaf.repo.YearsRepo;
import com.example.LoginSecurityThymeleaf.service.UserServiceImpl;

@Controller
@RequestMapping("/admin/")
public class AdminController {
	
	@Autowired
	private YearsRepo yearsRepo;
	@Autowired
	private MonthsRepo monthsRepo;
	@Autowired
	private  DepartmentRepo departmentRepo;
	@Autowired
	private EmployeeRepo empRepo;
	@Autowired
	private UserServiceImpl service;
	@Autowired
	private  RewardTypeRepo rewardTypeRepo;
	@Autowired
	private  RewardRepo  repo;
	
//	@GetMapping("list")
//	public String showUpdateForm(@Validated Employee employee,Model model) {
//	model.addAttribute("employees",	empRepo.findAll());
//	
//		return "index";
//	}
	
	// Employees -----1-----
	@GetMapping("listEmployees")
	public String ShowEmployeeList(Model model) {
		model.addAttribute("listEmployees",	empRepo.findAll());
	return "EmployeeTable";
	}
	
	@GetMapping("/addemp")
	public String addNewForm(Model model) {
		
		
		Department dept = new Department();
		model.addAttribute("employee", new Employee());
	
		model.addAttribute("listDepartment", departmentRepo.findAll());
		
		return "add-employeess";
		
	}
	
	@PostMapping("/addemployee")
    public String saveEmployees(Employee employee)
    {
		//Department dept = new Department();
		
		// if(dept.isStatus()==true) {
       
        empRepo.save(employee);
        
		// }
        return "redirect:listEmployees";
    }
	
	 @GetMapping("enableemp/{emp_Id}")
     public String enabledepartment(@PathVariable("emp_Id") Long emp_Id, Model model) {
     	Employee employee = empRepo.findById(emp_Id)
                 .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
         employee.setStatus("enable");
         empRepo.save(employee);
         model.addAttribute("listEmployees", empRepo.findAll());
         return "EmployeeTable";
         }
     
     
     @GetMapping("disableemp/{emp_Id}")        
     public String disableEmployee(@PathVariable("emp_Id") Long emp_Id,Model model) {
     	Employee employee = empRepo.findById(emp_Id)
                 .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
     	employee.setStatus("disable");
         
         empRepo.save(employee);
         model.addAttribute("listEmployees", empRepo.findAll());
         return "EmployeeTable";
     }
     
     @GetMapping("edit/{emp_Id}")
     public String showUpdateForm(@PathVariable("emp_Id") Long emp_Id, Model model) {
         Employee employee = empRepo.findById(emp_Id)
             .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
         model.addAttribute("employee", employee);
         model.addAttribute("listDepartment", departmentRepo.findAll());
         return "update";
     }

     @PostMapping("update")
     public String saveUpdatedEmployees(Employee employee) {

         empRepo.save(employee);
        
         return "redirect:listEmployees";
     } 
	
     //----------1-----------
     
     //Department--------2----
	@GetMapping("dept")
    public String showDepartmentForm(Department department) {
        return "add-Department";
    }
	
	
	@GetMapping("departmentlist")
	public String ShowDepartmentList(@Validated Department departmentList,Model model) {
	model.addAttribute("department",	departmentRepo.findAll());
			return "DepartmentList";
	}
	

	@PostMapping("/adddepartment")
    public String saveDepartment(Department dept)
    {
       
        departmentRepo.save(dept);
        return "redirect:departmentlist";
    }
	
	 @GetMapping("disable/{dept_Id}")        
     public String deletedept(@PathVariable("dept_Id") Long dept_Id,Model model) {
     	Department dept = departmentRepo.findById(dept_Id)
                 .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + dept_Id));
         dept.setStatus("disable");
         departmentRepo.save(dept);
         //empRepo.save(employee);
         model.addAttribute("department", departmentRepo.findAll());
         return "DepartmentList";
     }
     
    
     
     @GetMapping("enable/{dept_Id}")
     public String enableEmployee(@PathVariable("dept_Id") Long dept_Id, Model model) {
     	Department dept = departmentRepo.findById(dept_Id)
                 .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + dept_Id));
         dept.setStatus("enable");
         departmentRepo.save(dept);
         model.addAttribute("department", departmentRepo.findAll());
         return "DepartmentList";
         }
     
     @GetMapping("editdept/{emp_Id}")
     public String showUpdatedept(@PathVariable("emp_Id") Long dept_Id, Model model) {
         Department department = departmentRepo.findById(dept_Id)
             .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + dept_Id));
         model.addAttribute("department", department);
      
         return "update-dept";
     }

  
     @PostMapping("updatedept")
     public String saveUpdateddept(Department department) {

         departmentRepo.save(department);
        
         return "redirect:departmentlist";
     } 
     
     
     //----------2---------------
     
     
     
     //RewardType--------3----------
     
     @GetMapping("add-reward")
     public String showRewardForm(RewardType rewardType) {
         return "AddRewardType";
     }
 	
     @GetMapping("rewardtype")
 	public String Showrewardtype(RewardType rewardType,Model model) {
 	model.addAttribute("rewardtype",rewardTypeRepo.findAll());
 		return "RewardType";
 	}
     
     @PostMapping("getreward")
     public String saveReward(RewardType rewardType )
         {
   			rewardTypeRepo.save(rewardType);
             return "redirect:rewardtype";
         }

     @GetMapping("disableRT/{R_Id}")        
     public String disableRewardtype(@PathVariable("R_Id") Long R_Id,Model model) {
    	 RewardType rewardType= rewardTypeRepo.findById(R_Id)
                 .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + R_Id));
    	// rewardType.setStatus("disable");
         rewardTypeRepo.save(rewardType);
         //empRepo.save(employee);
         model.addAttribute("rewardType", rewardTypeRepo.findAll());
         return "RewardList";
     }
     
    
     
     @GetMapping("enableRT/{R_Id}")
     public String enableRewardtype(@PathVariable("R_Id") Long R_Id, Model model) {
    	 RewardType rewardType= rewardTypeRepo.findById(R_Id)
                 .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + R_Id));
    	// rewardType.setStatus("enable");
         rewardTypeRepo.save(rewardType);
         model.addAttribute("rewardType", rewardTypeRepo.findAll());
         return "RewardList";
         }
     
     @GetMapping("editRT/{emp_Id}")
     public String EditRewardtype(@PathVariable("R_Id") Long R_Id, Model model) {
    	 RewardType rewardType = rewardTypeRepo.findById(R_Id)
             .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + R_Id));
         model.addAttribute("rewardType", rewardType);
      
         return "update-rewardType";
     }

  
     @PostMapping("updateRT")
     public String saveRewardtype(RewardType rewardType) {

         rewardTypeRepo.save(rewardType);
        
         return "RewardList";
     } 
     
     
     
     
     
     
     
	
//	@GetMapping("rewards")
//	public String Showrewards(@Validated Rewards rewards,Model model) {
//	model.addAttribute("rewards",	rewardsRepo.findAll());
//
//
//		return "RewardList";
//	}
//	@RequestMapping(value = "/add", method = {RequestMethod.GET,RequestMethod.POST})
	@PostMapping("/add")
//    public String saveEmployee( Long emp_Id,String emp_Name,
//    	String reward_Type,
//    		 String dept,
//    		 MultipartFile image) throws IOException
//    {
//		
//		Employee emp=new Employee();
//		emp.setEmp_Id(emp_Id);
//        emp.setEmp_Name(emp_Name);
//        //emp.setReward_Type(reward_Type);
//       // emp.setDept(dept);
//		emp.setImage(Base64.getEncoder().encodeToString(image.getBytes()));
//		empRepo.save(emp);
//    	//empService.saveEmployeeToDB(emp_Id, emp_Name,reward_Type, dept,image);
//    	return "redirect:list";
//    }
//	
	
	
	
	
        @GetMapping("deleteemp/{emp_Id}")
        public String deleteEmployee(@PathVariable("emp_Id") Long emp_Id, Model model) {
        	Employee employee = empRepo.findById(emp_Id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + emp_Id));
        	empRepo.delete(employee);
            model.addAttribute("listEmployees", empRepo.findAll());
            return "EmployeeTable";
        }
        
     
        
       
        @GetMapping("rewards")
    	public String ShowRewardList( Rewards rewards,Model model) {
    	//List<Department> department=(List<Department>) departmentRepo.findAll();
       
    	model.addAttribute("listrewards",repo.findAll());
    	//model.addAttribute("department",departmentRepo.findAll());

    		return "RewardList";
    	}
        
        
        @GetMapping("addrewards")
        public String showRewards(Rewards rewards,Model model) {
        	model.addAttribute("rewards", new Rewards());
    		model.addAttribute("listRewards", rewardTypeRepo.findAll());
    		model.addAttribute("listEmployee", empRepo.findAll());
    		model.addAttribute("month", monthsRepo.findAll());
    		model.addAttribute("years", yearsRepo.findAll());
            return "add-rewards";
        }
    	
       
           @PostMapping("postrewards")
            public String saveRewards(Rewards rewards)
                {
            	repo.save(rewards);
                    return "redirect:rewards";
                }
          
           //@RequestMapping(value = "/delete", method = RequestMethod.POST)
           //delete emp
//           @PostMapping("delete")
//           public String delete(Model model) {
//               empRepo.delete(null);
//               model.addAttribute("employees", empRepo.findAll());
//               return "EmployeeTable";
//           }
//           
           //delete rewards
//           @PostMapping("deleterewards")
//           public String deleteRewards(Model model) {
//               repo.delete(null);
//               model.addAttribute("rewards", repo.findAll());
//               return "RewardsList";
//           }
//           //delete deopt
//           @PostMapping("deletedept")
//           public String deleteDept(Model model) {
//               departmentRepo.delete(null);
//               model.addAttribute("department", departmentRepo.findAll());
//               return "DepartmentList";
//           }
//           
          
           @GetMapping("editrewards/{Id}")
           public String showrewards(@PathVariable("Id") Long Id, Model model) {
               Rewards rewards = repo.findById(Id)
                   .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + Id));
              model.addAttribute("rewards", new Rewards());
       		model.addAttribute("listRewards", rewardTypeRepo.findAll());
       		model.addAttribute("listEmployee", empRepo.findAll());
       		model.addAttribute("month", monthsRepo.findAll());
       		model.addAttribute("years", yearsRepo.findAll());
               return "updaterewards";
           }

           @PostMapping("updaterewards")
           public String saveUpdatedrewards(Rewards rewards) {

               repo.save(rewards);
              
               return "RewardList";
           } 
           
           
           ////email
           
           @GetMapping("emailpage")
            public String Email() {
       		
        	   return "email1";
       	}
           
           
           
        
}
