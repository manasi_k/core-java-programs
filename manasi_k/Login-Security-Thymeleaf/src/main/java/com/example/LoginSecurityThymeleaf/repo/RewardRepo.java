package com.example.LoginSecurityThymeleaf.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.LoginSecurityThymeleaf.model.Department;
import com.example.LoginSecurityThymeleaf.model.Rewards;


public interface RewardRepo extends JpaRepository<Rewards, Long> {

}
