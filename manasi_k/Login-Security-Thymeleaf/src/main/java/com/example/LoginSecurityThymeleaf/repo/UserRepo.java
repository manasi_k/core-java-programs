package com.example.LoginSecurityThymeleaf.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.multipart.MultipartFile;

import com.example.LoginSecurityThymeleaf.model.User;

public interface UserRepo extends JpaRepository<User, Long>{
	 User findByEmail(String email);
	
	}