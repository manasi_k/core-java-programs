package com.example.LoginSecurityThymeleaf.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.LoginSecurityThymeleaf.model.Department;
import com.example.LoginSecurityThymeleaf.model.Employee;
import com.example.LoginSecurityThymeleaf.model.RewardType;
import com.example.LoginSecurityThymeleaf.model.Rewards;
import com.example.LoginSecurityThymeleaf.model.Role;
import com.example.LoginSecurityThymeleaf.model.User;
import com.example.LoginSecurityThymeleaf.repo.EmployeeRepo;
import com.example.LoginSecurityThymeleaf.repo.RewardTypeRepo;
import com.example.LoginSecurityThymeleaf.repo.UserRepo;
import com.example.LoginSecurityThymeleaf.web.dto.UserRegDto;




@Service
public class UserServiceImpl implements UserService {
	@Autowired
    private UserRepo userRepository;
	@Autowired
    private EmployeeRepo employeeRepo;
	
	@Autowired
	private RewardTypeRepo repo;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepo userRepository) {
        super();
        this.userRepository = userRepository;
    }
    
    @Autowired
    private JavaMailSender javaMailSender;
    
    @Autowired
	public UserServiceImpl(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	
    
    
   
     public void send( String subject, String message) {

    	 Employee emp=new Employee();
    	 
    	 String Email=emp.getEmail();
     SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
    simpleMailMessage.setTo(Email);
    simpleMailMessage.setSubject(subject);
    simpleMailMessage.setText(message);
    javaMailSender.send(simpleMailMessage);
    }
    
    
    @Override
    public void sendEmail(Employee emp) throws MailException {

	
		SimpleMailMessage mail = new SimpleMailMessage();
		
		String empemail;
		empemail=emp.getEmail();
		mail.setTo(empemail);
		mail.setSubject("Testing Mail API");
		mail.setText("Hurray ! You have done that dude...");

		
		javaMailSender.send(mail);
	}
    
    
    
    @Autowired
    public JavaMailSender emailSender;

 

    public void sendSimpleEmail(String toAddress, String subject, String message) {

 

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(toAddress);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(message);
        emailSender.send(simpleMailMessage);
    }
    
    
    
    
    @Override
    public User save(UserRegDto registrationDto) {
        User user = new User(registrationDto.getFirstName(),
            registrationDto.getLastName(), registrationDto.getEmail(),
            passwordEncoder.encode(registrationDto.getPassword()), Arrays.asList(new Role("ROLE_USER")));

        return userRepository.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), mapRolesToAuthorities(user.getRoles()));
    }

    private Collection <? extends GrantedAuthority > mapRolesToAuthorities(Collection < Role > roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    }
   
	@Override
    public void  saveEmployeeToDB(Long emp_Id, String emp_Name, Long dept_Id)
	{
		Employee emp = new Employee();
		
		emp.setEmp_Id(emp_Id);
        emp.setEmp_Name(emp_Name);
        emp.setEmp_Id(dept_Id);
        //emp.setReward_Type(reward_Type);
       // emp.setDept(dept);
       
		
        	employeeRepo.save(emp);

	}
//	public void saveRewardToDB(Long r_Id, String reward_Type, Boolean status) {
//        RewardType rt =new RewardType();
//        rt.setR_Id(r_Id);
//        rt.setReward_Type(reward_Type);
//       rt.setStatus(status);
//        repo.save(rt);
//       
//    }

	public void saveRewards(Long r_Id, Long emp_Id) {
		Rewards rd=new Rewards();
		rd.setId(r_Id);
		rd.setId(emp_Id);
		// TODO Auto-generated method stub
		
	}


	@Override
	public void sendEmail(String recipient) throws MailException {
		// TODO Auto-generated method stub
		
	}
   
	
}
