package colletions;

public class emp implements Comparable<emp> {
	 private int id;
	 private String name;
	 private int age;
	 
	 @Override
	 public int compareTo(emp m) 
	    { 
	        return this.id - m.id; 
	    } 
	  
	 public emp(int id, String nm, int ag) 
	    { 
	        this.id = id; 
	        this.name = nm; 
	        this.age = ag; 
	    } 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	 
	@Override
    public String toString() {
        return "Employee : " + id + " - " + name + " - " + age + "\n";
    }

	}



