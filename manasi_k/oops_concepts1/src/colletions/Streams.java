package colletions;

import java.util.*;
import java.util.stream.Collectors;

public class Streams {

	public static void main(String[] args) {
		
		List<Integer> l1=Arrays.asList(1,2,3,4,5,6);
		
		List map=l1.stream().map(x-> x+x).collect(Collectors.toList());
		
		System.out.print(map);
		
		List<String> s1= Arrays.asList("manasi","shraddha","snehal","ashwini");
		
		List filter=s1.stream().filter(x-> x.startsWith("s")).collect(Collectors.toList());
		System.out.print(filter);
		
		List<String> sort = s1.stream().sorted().collect(Collectors.toList()); 
	    System.out.println(sort); 
	    
	    Optional<Integer> reduce=l1.stream().reduce((x,y) -> x < y ? x:y);
	    
	    System.out.println(reduce); 
		
	}

}
