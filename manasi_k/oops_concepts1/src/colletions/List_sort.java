package colletions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class List_sort {

	public static void main(String[] args) {
		Employee e1= new Employee(10,"shraddha",22);
		Employee e2= new Employee(12,"pooja",30);
		Employee e3= new Employee(1,"manasi",21);
		Employee e4= new Employee(89,"ashwini",18);
		Employee e5= new Employee(24,"snehal",25);
		
		  List<Employee> employees = new ArrayList<Employee>();
		  	employees.add(e2);
	        employees.add(e3);
	        employees.add(e1);
	        employees.add(e4);
	        employees.add(e5);
	        
	        Collections.sort(employees);
	        
	        System.out.println(employees);
	        
	        
	}

}
