package colletions;
import java.lang.Class;
import java.lang.reflect.*;


public class reflection {

	public static void main(String[] args) {
		

	//cat c=new cat();
	//Class obj=c.getClass();
	
	Class obj=cat.class;
	
	String name = obj.getName();
	System.out.println("name of class is "+ name);
	System.out.println("name of superclass"+obj.getSuperclass());
	System.out.println("name of package "+obj.getPackage());
	
	
	int modifier = obj.getModifiers();
	String mod = Modifier.toString(modifier);
    System.out.println("Modifier: " + mod);
    
    Field[] fields=obj.getFields();
    for(Field f:fields) {
    	System.out.println("Public Fields are " +f.getName());
    }
    
    Class superClass = obj.getSuperclass();
    System.out.println("Superclass: " + superClass.getName());
  
    Method[] methods = obj.getDeclaredMethods();
    for(Method m: methods) {
    	
    
    System.out.println("Method Name: " + m.getName());
    
    
    }
	
}
}