	package colletions;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Lambda_function {

	public static void main(String[] args) {
		
		List<Integer> l1=Arrays.asList(1,2,3,4,5,6);
		
		/*for(int i:l1) {
			System.out.print(i);
		}
*/
		/*
		Consumer<Integer> c1= new Consumer<Integer>()
				{
					public void accept(Integer i)
					{
						System.out.print(i);
					}
			
				};
				
				l1.forEach(c1);
				
				
				----- Consumer<Integer> c1= (Integer i) -> System.out.println(i);
		*/
		l1.forEach(i -> System.out.println(i) );
	}

}
