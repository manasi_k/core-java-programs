package colletions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialize {
	
	
	public static void main(String[] args) throws Exception {
		
		test t=new test();
		
		t.i=10;
		t.j=20;
		
		File f=new File("object_file.txt");
		
		FileOutputStream fs= new FileOutputStream(f);
		ObjectOutputStream os=new ObjectOutputStream(fs);
		os.writeObject(t);
		
		FileInputStream fis=new FileInputStream(f);
		ObjectInputStream ois=new ObjectInputStream(fis);
		test f1=(test) ois.readObject();
		
		System.out.println(f1.i);
		
	}
	
	
	
}
