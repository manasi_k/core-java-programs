package colletions;
import java.io.*; 
import java.util.*;


public class List_sort_2{

	public static void main(String[] args) 
	{ 
		ArrayList<emp> list = new ArrayList<emp>(); 
		list.add(new emp(10,"aisha",22)); 
		list.add(new emp(20,"mayuri",25)); 
		list.add(new emp(5,"preeti",3)); 
		list.add(new emp(3, "komal", 55)); 

		System.out.println("Sorted by ID"); 
		
		Collections.sort(list); 
		for (emp e: list) 
			System.out.println(e.getId() + " " + e.getName() + " " + e.getAge()); 

		System.out.println("Sorted by Name"); 
		compare_name C2 = new compare_name(); 
		Collections.sort(list, C2); 
		for (emp e: list) 
			System.out.println(e.getId() + " " + e.getName() + " " + e.getAge()); 
		
		System.out.println("Sorted by AGE"); 
		compare_age C3 = new compare_age(); 
		Collections.sort(list, C3); 
		for (emp e: list) 
			System.out.println(e.getId() + " " + e.getName() + " " + e.getAge()); 

		
	}
}