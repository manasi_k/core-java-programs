package colletions;

import java.util.Comparator;

public class compare_name implements Comparator<emp>{
	
	public int compare(emp m1, emp m2) 
	{ 
		return m1.getName().compareTo(m2.getName()); 
	} 
}
