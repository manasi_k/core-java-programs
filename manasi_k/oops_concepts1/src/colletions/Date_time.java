package colletions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;

public class Date_time {

	public static void main(String[] args) {
		
		LocalDate l1=LocalDate.now();
		System.out.println(l1);
		
		
		LocalDate l2=LocalDate.of(2020,Month.NOVEMBER,18);
		System.out.println(l2);
		
		LocalDateTime l3=LocalDateTime.now();
		System.out.println(l3);
		
		LocalTime l4=LocalTime.now();
		System.out.println(l4);
		
		LocalTime l5=LocalTime.now(ZoneId.of("America/New_York"));
		System.out.println(l5);
		
/*		for (String s: ZoneId.getAvailableZoneIds())
		{
			System.out.println(s);
		}
*/		
	}

}
