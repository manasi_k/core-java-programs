package oops_concepts;

public class Poly {

	public int add(int a , int b) {
		return a+b;
	}
	
	public int add(int a, int b, int c) {
		return a+b+c;
	}
	public double add(double a, double b, double c) {
		return a+b+c;
	}
	public static void main(String[] args) {
		Poly a= new Poly();
		
		System.out.println(a.add(10,20));
		System.out.println(a.add(20.2,30.22, 50.4));
		System.out.println(a.add(10, 20, 30));
	}

}
