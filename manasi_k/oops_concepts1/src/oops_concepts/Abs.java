package oops_concepts;

abstract class tree{
	public abstract void height();
	void colorLeaves() {
		System.out.println("color of leaves is green in colour");
	}
}

class mango extends tree{
	public void height() {
		System.out.println("Mango Tree is tall");
	}
}
public class Abs {
 
	public static void main(String[] args) {
		mango m=new mango();
		m.height();
		//m.colorLeaves();
	}

}
