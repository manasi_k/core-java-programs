package oops_concepts;

interface tree1{
	public void height();
	public void colorleaves();
	
}

class mango1 implements tree1{
	public void height() {
		System.out.print("Mango tree is tall");
	}
	public void colorleaves() {
		System.out.print("color of leaves is green");
	}
}
public class Inter {

	public static void main(String[] args) {
		mango1 m=new mango1();
		m.colorleaves();
		m.height();
	}

}
