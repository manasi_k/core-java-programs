package oops_concepts;

class parent{
	int parent_age=80;
}

class child extends parent{
	int child_age=30;
}
class grandchild extends child{
	int g_child_age=5;
}
public class Inherit {
	
	public static void main(String[] args) {
	grandchild g=new grandchild();
	System.out.println("PARENTS AGE IS: "+g.parent_age);
	System.out.println("CHILD AGE IS: "+g.child_age);
	System.out.println("G_CHILD AGE IS: "+g.g_child_age);
	}

}
