package com.example.module;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Employee {
	
	
	@Id 
	private String emp_Id;
	  private String emp_Name;
	  private String reward_Type;
	  private String dept;
	  
}