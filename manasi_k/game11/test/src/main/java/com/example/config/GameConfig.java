package com.example.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.example.GuessCount;
import com.example.MaxNumber;

@Configuration
@ComponentScan(basePackages = "com.example")
@PropertySource("classpath:config/game.properties")
public class GameConfig {
	
	@Value("${game.maxNumber}")
	private int maxNumber;
	@Value("${game.guessCount}")
	private int guessCount;
	
	@Bean
	@MaxNumber
	public int maxNumber() {
		return maxNumber;
	}

	@Bean
	@GuessCount
	public int guessCount() {
		return guessCount;
	}

}
