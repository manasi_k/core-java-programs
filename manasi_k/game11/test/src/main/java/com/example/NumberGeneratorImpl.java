package com.example;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;


@Component
public class NumberGeneratorImpl implements NumberGenerator {

    // == fields ==
    private final Random random = new Random();

   @Getter
    private int maxNumber;
    
    
    @Autowired
    public NumberGeneratorImpl(@MaxNumber int maxNumber) {
		super();
		this.maxNumber = maxNumber;
	}

	// == public methods ==
    @Override
    public int next() {
        return random.nextInt(maxNumber);
    }

    
}
