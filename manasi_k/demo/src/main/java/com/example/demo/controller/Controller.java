package com.example.demo.controller;

import java.util.List;

import javax.management.AttributeNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Student;
import com.example.demo.repository.Repository;
import com.example.demo.services.Services;

@RestController
public class Controller {
	@Autowired
//	Services ser;
	
	private Repository repo;

	@PostMapping("/add")
	public String Studentadd(@RequestBody List<Student> student) {
		repo.saveAll(student);
		return "student saved:  "+student.size();
	}
	
	
	
	@GetMapping("/show")
	private List<Student> getdetails(){
		return (List<Student>) repo.findAll();
		
	}	
	
	@DeleteMapping("/delete/{id}")
  void deleteStudent(@PathVariable("id") int id) {
    repo.deleteById(id);
  
	}
		
	@PutMapping("/update/{id}")
    public ResponseEntity<Student> updatestudent( @PathVariable(value = "id") int Id,	 @RequestBody Student s1) throws AttributeNotFoundException {
         Student student = repo.findById(Id)
          .orElseThrow(() -> new AttributeNotFoundException("User not found on :: "+ Id));
  
        student.setName(s1.getName());
        student.setSubject(s1.getSubject());
        
        final Student updatedstudent = repo.save(student);
        return ResponseEntity.ok(updatedstudent);
   }
}	
/*
	
	@GetMapping("/students")
	private List<Student> getdetails()
	{
		return ser.getdetails();
	}
		
	
	@GetMapping("/s/{s_id}")
	private Student getStudents(@PathVariable("s_id")int s_id)
	{
			return ser.getStudentsbyId(s_id);
	}
	
	@DeleteMapping("/delStudent/{del_id}")  
	private void deleteStudent(@PathVariable("del_id") int del_id)   
	{  
	ser.delete(del_id);  
	}  
	
	@PostMapping("/add_student")  
	private String saveStudent(@RequestBody List<Student> student)   
	{  
	ser.saveOrUpdate(student);  
	return "student saved:  "+student.size();
	}  

	@PutMapping("/update")  
	private String update(@RequestBody List<Student> student)   
	{  
	ser.saveOrUpdate(student);  
	return "student Updated:  "+student.size();
	} 
	
	
}
*/