package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Student;
import com.example.demo.repository.Repository;

@Service
public class Services {

	@Autowired
	Repository repo;
	
	public List<Student> getdetails()
	{
		List<Student> student=new ArrayList<Student>();
		repo.findAll().forEach(student1->student.add(student1));
		return student;
		
		
	}
	
	public Student getStudentsbyId(int id)
	{
		return repo.findById(id).get();
	}
	
	
	public void delete(int id)   
	{  
	repo.deleteById(id);  
	}  
	
	public void saveOrUpdate(List<Student> student)   
	{  
	repo.saveAll(student);  
	}  
}
