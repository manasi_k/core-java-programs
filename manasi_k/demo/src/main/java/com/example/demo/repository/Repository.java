package com.example.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.*;

import com.example.demo.entity.Student;

public interface Repository extends JpaRepository<Student, Integer>
{

}
