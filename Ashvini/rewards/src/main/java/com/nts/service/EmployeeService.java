package com.nts.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nts.entity.Employee;
import com.nts.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	EmployeeRepository empRepository;

	public void Save(Employee employee) {

		employee.setEmp_Id(employee.getEmp_Id());
		employee.setEmp_Name(employee.getEmp_Name());
		employee.setReward_Type(employee.getReward_Type());
		employee.setDept(employee.getDept());

		//employee.setImage(Base64.getEncoder().encodeToString((byte[]) employee.getImage( )));
		employee.setImage(employee.getImage());
		// data1=employee.getData1();
		//String data1 =(Base64.getEncoder().encodeToString(employee.getImage()));

		//System.out.println(data1);
		empRepository.save(employee);

	}
	public void findAllEmployees(Employee employee) {
		
		employee.getEmp_Id();
		employee.getEmp_Name();
		employee.getReward_Type();
		employee.getDept();
		employee.getData1();
		empRepository.findAll();		
	}
	
        }


