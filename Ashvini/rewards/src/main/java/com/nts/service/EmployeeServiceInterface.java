package com.nts.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.nts.entity.Employee;

public interface EmployeeServiceInterface {
	
	public void   saveEmployeeToDB(long sr_No,String emp_Id, String emp_Name,String reward_Type,String dept);

	void saveEmployeeToDB(List<Employee> employee);

}
