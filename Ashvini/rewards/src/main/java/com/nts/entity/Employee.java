package com.nts.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;



@Entity
@Table(name="NewEmployee")
public class Employee {
	
	
	@Id 
	private String emp_Id;
	  private String emp_Name;
	  private String reward_Type;
	  private String dept;
	 @Lob
		private byte[] image;
	 @Transient
	 public Object data1;
	 
	public Object getData1() {
		return data1;
	}
	public void setData1(Object data1) {
		this.data1 = data1;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getEmp_Id() {
		return emp_Id;
	}
	public void setEmp_Id(String emp_Id) {
		this.emp_Id = emp_Id;
	}
	public String getEmp_Name() {
		return emp_Name;
	}
	public void setEmp_Name(String emp_Name) {
		this.emp_Name = emp_Name;
	}
	public String getReward_Type() {
		return reward_Type;
	}
	public void setReward_Type(String reward_Type) {
		this.reward_Type = reward_Type;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	  
	  
}