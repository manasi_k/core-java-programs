package com.nts.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.nts.entity.Employee;



@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
    
   
    
}
