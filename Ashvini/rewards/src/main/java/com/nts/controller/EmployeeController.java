package com.nts.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nts.entity.Employee;
import com.nts.repository.EmployeeRepository;
import com.nts.service.EmployeeService;


@Controller
@RequestMapping("/employees/")
public class EmployeeController {
	@Autowired
	EmployeeService empService;
	@Autowired
	private  EmployeeRepository employeeRepository;
	
	@Autowired
	public EmployeeController(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	@GetMapping("signup")
	public String showSignUpForm(Employee employee) {
		return "add-employee";
	}

	@GetMapping("list")
	public String showUpdateForm(@Validated Employee employee,Model model) {
	model.addAttribute("employees",	employeeRepository.findAll());


		return "index";
	}
	
	@RequestMapping(value="add" ,method = { RequestMethod.GET, RequestMethod.POST })
	public String addEmployee(@Validated Employee employee, BindingResult result, String data1,Model model) {
		
		empService.Save(employee);

		return "redirect:list";
	}
	//	@PostMapping("add")
//	public String addEmployee(@Validated Employee employee, BindingResult result, String data1,Model model) {
//		
//		empService.Save(employee);
//
//		return "redirect:list";
//	}

	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Employee employee = employeeRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		model.addAttribute("employee", employee);
		return "update-employee";
	}
//
//	@PostMapping("update/{id}")
//	public String updateStudent(@PathVariable("id") long id, @Validated Employee employee, BindingResult result,
//			Model model) {
//		if (result.hasErrors()) {
//			employee.setId(id);
//			return "update-student";
//		}
//
//		employeeRepository.save(employee);
//		model.addAttribute("students", employeeRepository.findAll());
//		return "index";
//	}
//
//	@GetMapping("delete/{id}")
//	public String deleteStudent(@PathVariable("id") long id, Model model) {
//		Employee employee = employeeRepository.findById(id)
//				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
//		employeeRepository.delete(employee);
//		model.addAttribute("students", employeeRepository.findAll());
//		return "index";
//	}
}
