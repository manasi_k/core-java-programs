<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<html>
<head>
<title>First Web Application</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">

</head>

<body>
	<div class="container">

	<form:form method="post" modelAttribute="todo">
      <form:hidden path="id" />
      
     <!--  <fieldset class="form-group">
				<form:label path="userName">User</form:label> 
				<form:input path="userName" type="text"
					class="form-control" required="required"/>
					<form:errors path="userName" cssClass="text-warning"/>
					
			</fieldset> -->
      <fieldset class="form-group">
       <form:label path="description">Description</form:label>
       <form:input path="description" type="text" class="form-control"
        required="required" />
       <form:errors path="description" cssClass="text-warning" />
      </fieldset>
			<fieldset class="form-group">
				<form:label path="taskAssignedDate">AssignedDate</form:label> 
				<form:input path="taskAssignedDate" type="text"
					class="form-control" required="required"/>
					<form:errors path="taskAssignedDate" cssClass="text-warning"/>
					
			</fieldset>
			<fieldset class="form-group">
				<form:label path="taskUpdatedDate">UpdatedDate</form:label> 
				<form:input path="taskUpdatedDate" type="text"
					class="form-control" required="required"/>
					<form:errors path="taskUpdatedDate" cssClass="text-warning"/>
					
			</fieldset>
			<fieldset class="form-group">
				<form:label path="targetDate">TargetDate</form:label> 
				<form:input path="targetDate" type="text"
					class="form-control" required="required"/>
					<form:errors path="targetDate" cssClass="text-warning"/>
					
			</fieldset>
			
			<button type="submit" class="btn btn-success">Add</button>
		</form:form>
	</div>

	<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="webjars/bootstrap-datepicker/1.0.1/js/bootstrap-datepicker.js"></script>


	<script>
    $(function () {
        $("#targetDate").datepicker();
    });
</script>
<script>
    $(function () {
        $("#taskAssignedDate").datepicker();
    });
</script>
<script>
    $(function () {
        $("#taskUpdatedDate").datepicker();
    });
</script>
</body>

</html>
