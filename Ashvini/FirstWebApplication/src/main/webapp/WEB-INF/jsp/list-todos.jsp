<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<html>

<head>
<title>Todo's for ${name}</title>
<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css"
	rel="stylesheet">
</head>

<body>

<nav role="navigation" class="navbar navbar-default">

	<div class="navbar-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="/">Home</a></li>
	<li><a href="/list-todos" > Todos </a> </li>
		</ul>
	</div>
</nav>
	<div class="container">
		<table class="table table-striped">
			<caption>
			<H1>Your todos are </H1></caption>
			<thead>
				<tr>
					<th>Id</th>
					<th>User</th>
					<th>Description</th>
					<th>AssignedDate</th>
					<th>UpdatedDate</th>
					<th>Target Date</th>
					<th>Is it Done?</th>
				<<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${todos}" var="todo">
					<tr>
						<td>${todo.id}</td>
						<td>${todo.userName}</td>
						<td>${todo.description}</td>
						<td><fmt:formatDate value="${todo.taskAssignedDate}" pattern="dd/MM/yyyy"/></td>
						<td><fmt:formatDate value="${todo.taskUpdatedDate}" pattern="dd/MM/yyyy"/></td>
						<td><fmt:formatDate value="${todo.targetDate}" pattern="dd/MM/yyyy"/></td>
						<td>${todo.done}</td>
						<td><a type="button" class="btn btn-success"
							href="/update-todo?id=${todo.id}">Update</a></td>
						<td><a type="button" class="btn btn-warning"
							href="/delete-todo?id=${todo.id}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div> <a class="button" href="/add-todo">Add a Todo</a></div>
		<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
	    <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		
	</div>
</body>

</html>