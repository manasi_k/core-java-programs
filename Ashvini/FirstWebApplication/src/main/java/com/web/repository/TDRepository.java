package com.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.model.TD;

public interface TDRepository extends JpaRepository < TD, Long > {
	

	   List < TD > findByUserName(String user);

	}


