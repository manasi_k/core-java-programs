package com.web.service;


import java.util.Date;

import java.util.List;
import java.util.Optional;

import com.web.model.TD;


public interface TDServiceInterface {

	List<TD> get();

    List < TD > getTodosByUser(String user);
   // List < TD > getTodos();
    Optional < TD > getTodoById(long id);

    void updateTodo(TD todo);

    void addTodo(Long id,String userName, String description, Date taskAssignedDate, Date taskUpdatedDate,Date targetDate, boolean isDone);

    void deleteTodo(long id);

    void saveTodo(TD todo);
    
    //List Show();
}