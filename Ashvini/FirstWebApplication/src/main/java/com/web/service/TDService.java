package com.web.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.model.TD;
import com.web.repository.TDRepository;


@Service
public class TDService implements TDServiceInterface {

    @Autowired
    private TDRepository todoRepository;
    
  

    /* @Override
    public List < TD > getTodos() {
        return todoRepository.findAll();
    }*/

 @Override
    public List <TD > getTodosByUser(String user) {
        return todoRepository.findByUserName(user);
    }

    @Override
    public Optional < TD > getTodoById(long id) {
        return todoRepository.findById(id);
    }

    @Override
    public void updateTodo(TD todo) {
        todoRepository.save(todo);
    }

    @Override
    public void addTodo(Long id,String userName, String description,Date taskAssignedDate, Date taskUpdatedDate, Date targetDate, boolean isDone) {
        todoRepository.save(new TD(id,userName, description,taskAssignedDate,  taskUpdatedDate ,targetDate, isDone));
    }

    @Override
    public void deleteTodo(long id) {
        Optional < TD > todo = todoRepository.findById(id);
        if (todo.isPresent()) {
            todoRepository.delete(todo.get());
        }
    }

    @Override
    public void saveTodo(TD todo) {
        todoRepository.save(todo);
    }

	@Override
	public List<TD> get() {
		// TODO Auto-generated method stub
		return null;
	}




}