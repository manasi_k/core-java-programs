package com.web.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;


@Entity
@Table(name = "todos")
public class TD {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String userName;

    @Size(min = 10, message = "Enter at least 10 Characters...")
    private String description;

    private Date targetDate;
    
    private Date taskAssignedDate;
    private Date taskUpdatedDate;
   

	private boolean isDone;
    

    public TD() {
        super();
    }

    public TD(Long id,String userName, String description, Date taskAssignedDate, Date taskUpdatedDate, Date targetDate, boolean isDone) {
        super();
        this.id = id;
        this.userName = userName;
        this.description = description;
        this.taskAssignedDate = taskAssignedDate;
        this.taskUpdatedDate =taskUpdatedDate;
        this.targetDate = targetDate;
        this.isDone = isDone;
    }
    


    public Date getTaskAssignedDate() {
		return taskAssignedDate;
	}

	public void setTaskAssignedDate(Date taskAssignedDate) {
		this.taskAssignedDate = taskAssignedDate;
	}

	public Date getTaskUpdatedDate() {
		return taskUpdatedDate;
	}

	public void setTaskUpdatedDate(Date taskUpdatedDate) {
		this.taskUpdatedDate = taskUpdatedDate;
	}

	public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }
    
    public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TD other = (TD) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format(
                "TD [id=%s, userName=%s, description=%s, taskAssignedDate = %s,taskUpdatedDate = %s, targetDate=%s, isDone=%s]", id,
                userName, description,taskAssignedDate,taskUpdatedDate, targetDate, isDone);
    }

}
