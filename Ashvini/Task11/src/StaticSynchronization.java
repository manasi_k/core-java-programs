class Table2{  
  
 synchronized static void printTable(int n){  
   for(int i=1;i<=10;i++){  
     System.out.println(n*i);  
     try{  
       Thread.sleep(400);  
     }catch(Exception e){}  
   }  
 }  
}  
  
class ThreadA extends Thread{  
public void run(){  
Table2.printTable(1);  
}  
}  
  
class ThreadB extends Thread{  
public void run(){  
Table2.printTable(10);  
}  
}  
  
class ThreadC extends Thread{  
public void run(){  
Table2.printTable(100);  
}  
}  
  
  
  
  
class ThreadD extends Thread{  
public void run(){  
Table2.printTable(1000);  
}  
}  
  
public class StaticSynchronization{  
public static void main(String t[]) throws InterruptedException{  
ThreadA t1=new ThreadA();  
ThreadB t2=new ThreadB();  
ThreadC t3=new ThreadC();  
ThreadD t4=new ThreadD();  
t1.start();
t1.join();

t2.start(); 
t2.join();

t3.start();  
t3.join();

t4.start();
t4.join();
}  
}  