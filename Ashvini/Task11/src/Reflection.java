
import java.lang.reflect.Method; 
import java.lang.reflect.Field; 
import java.lang.reflect.Constructor; 

  class Abc{
	void printName(){
	 Object obj = new Object();
	Class c=obj.getClass();    
	  System.out.println(c.getName());  
   
}
	}
class B extends Abc{}
interface A{}  
  
class Reflection
{  
 public static void main(String args[]) throws ClassNotFoundException
 {  
	 Abc abc =new Abc();
	  B b =new B();
	 Class c = b.getClass();   
	 Method[] methods = c.getMethods(); 
	 
	//abc.getDeclaredMethod("printName", int.class);

     // Printing method names 
	 System.out.println("Public methods of the class are"); 
	    
     for (Method method:methods) 
         System.out.println(method.getName()); 
    
	   System.out.println(c.getName());  
	   System.out.println("SuperClass of Class is"+c.getSuperclass());  
	   
	// Creates object of the desired method by providing 
       // the name of method as argument to the  
       // getDeclaredMethod method 
       
   Class c1=Class.forName("Abc");  
   System.out.println(c1.isInterface());
   System.out.println("SuperClass of Abc is"+c1.getSuperclass());  

     
   Class c2=Class.forName("B");  
   System.out.println(c2.isInterface());  
   System.out.println(c2.getSuperclass());  

   	abc.printName();
 }  
}  
