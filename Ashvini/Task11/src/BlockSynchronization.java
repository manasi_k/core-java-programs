class Triplet
{  
  
 void printTriplet(int n)
 {  
   synchronized(this){//synchronized block  
     for(int i=1;i<=20;i++){  
    	 int result =(n*3);  
    	 System.out.println(result);
    	 n = result;
      try{  
       Thread.sleep(400);  
      }catch(Exception e){System.out.println(e);}  
     }  
   }  
 }//end of the method  
}  
  
class Threadt1 extends Thread{  
Triplet t;  
Threadt1(Triplet t){  
this.t=t;  
}  
public void run(){  
t.printTriplet(5);  
}  
  
}  
class Threadt2 extends Thread{  
Triplet t;  
Threadt2(Triplet t){  
this.t=t;  
}  
public void run(){  
t.printTriplet(25);  
}  
}  
  
public class BlockSynchronization{  
public static void main(String args[]){  
Triplet obj = new Triplet();//only one object  
Threadt1 tt1 = new Threadt1(obj);
Threadt2 tt2 = new Threadt2(obj);
tt1.start();  
tt2.start();  
}  
}  