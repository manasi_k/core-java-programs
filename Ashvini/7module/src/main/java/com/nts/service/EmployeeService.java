package com.nts.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nts.entity.Employee;
import com.nts.repository.EmployeeRepository;

@Service
public class EmployeeService {
	@Autowired
	EmployeeRepository empRepository;

	public void Save(Employee employee) {

		employee.setEmp_Id(employee.getEmp_Id());
		employee.setEmp_Name(employee.getEmp_Name());
		employee.setDept(employee.getDept());
		employee.setReward_Type(employee.getReward_Type());
		
		//employee.setImage(Base64.getEncoder().encodeToString((byte[]) employee.getImage( )));
		employee.setImage(employee.getImage());
		//employee.setData(data);
		empRepository.save(employee);
		String data=(Base64.getEncoder().encodeToString(employee.getImage()));

	}
	
        }


