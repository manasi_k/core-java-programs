
public class StringBuilderAndBuffer
{ 
	
	// Concatenates to StringBuilder 
	public static void concat1(StringBuilder s1) 
	{ 
		s1.append("StringBuilder"); 
	} 

	// Concatenates to StringBuffer 
	public static void concat2(StringBuffer s2) 
	{ 
		s2.append("StringBuffer"); 
	} 

	public static void main(String[] args) 
	{ 
		StringBuilder s1 = new StringBuilder("Use of "); 
		concat1(s1); // s1 is changed 
		System.out.println("StringBuilder: " + s1); 

		StringBuffer s2 = new StringBuffer("Use of "); 
		concat2(s2); // s2 is changed 
		System.out.println("StringBuffer: " + s2); 
	} 
} 
