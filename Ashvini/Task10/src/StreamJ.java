import java.util.*;  
import java.util.stream.Collectors;  
class Product{  
    int id;  
    String name;  
    float price;  
    public Product(int id, String name, float price) {  
        this.id = id;  
        this.name = name;  
        this.price = price;  
    }  
}  
public class StreamJ {  
    public static void main(String[] args) {  
        List<Product> productsList = new ArrayList<Product>();  
        //Adding Products  
        productsList.add(new Product(1," Laptop",25000f));  
        productsList.add(new Product(2,"Android Mobile",10000f));  
        productsList.add(new Product(3,"Android TV",40000f));  
        productsList.add(new Product(4,"Purifier",15000f));  
       
        List<Float> productPriceList2 =productsList.stream()  
                                     .filter(p -> p.price > 20000)// filtering data  
                                     .map(p->p.price)        // fetching price  
                                     .collect(Collectors.toList()); // collecting as list  
        System.out.println(productPriceList2);  
        
        productsList.stream()  
        .filter(product -> product.price == 40000)  
        .forEach(product -> System.out.println(product.name));    
        
        float totalPrice2 = productsList.stream()  
                .map(product->product.price)  
                .reduce(0.0f,Float::sum);   // accumulating price, by referring method of Float class  
        System.out.println(totalPrice2);  
          

    }  
}  