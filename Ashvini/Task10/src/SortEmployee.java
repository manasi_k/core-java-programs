import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
 
class Employee{
    String name;
    String id;
    Integer age;
    public Employee() {}
   
    public Employee(String name, String id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }
   
   
    @Override
    public String toString() {
        return "Employee{" + "name=" + name + ", id=" + id  + ",age="+age+'}';
    }
 
    
    class ComparatorName  implements Comparator<Employee>{
       @Override
        public int compare(Employee obj1, Employee obj2) {
           //sort Employee on basis of name(ascending order)
           return obj1.name.compareTo(obj2.name);
        }
    }
   
 
    class ComparatorId  implements Comparator<Employee>{
       @Override
        public int compare(Employee obj1, Employee obj2) {
           //sort Employee on basis of id(ascending order)
           return obj1.id.compareTo(obj2.id);
        }
    }
    
    class ComparatorAge  implements Comparator<Employee>{
        @Override
         public int compare(Employee obj1, Employee obj2) {
            //sort Employee on basis of age(ascending order)
            return obj1.age.compareTo(obj2.age);
         }
     }
 
}
 
 
 
public class SortEmployee{
    public static void main(String[] args) {
 
        Employee emp1=new Employee("Ashvini","N001",22);
        Employee emp2=new Employee("Manasi","N004",21);
        Employee emp3=new Employee("Snehal","N003",22);

        ArrayList<Employee> list=new ArrayList<Employee>();
        list.add(emp1);
        list.add(emp2);
        list.add(emp3);
      
        System.out.println("list Before sorting : \n"+list);
 
        Collections.sort(list,new Employee().new ComparatorName());
        System.out.println("\nlist after sorting on basis of name(ascending order), \n"+ list);
 
        Collections.sort(list,new Employee().new ComparatorId());
        System.out.println("\nlist after sorting on basis of id(ascending order),  \n"+list);
       
        Collections.sort(list,new Employee().new ComparatorAge());
        System.out.println("\nlist after sorting on basis of id(ascending order),  \n"+list);
       
    }
}