import java.util.*;
public class SortHashMap {
   public static void main(String args[]) {
      HashMap<String,Integer> hm = new HashMap();
      hm.put("Shirts", 700);
      hm.put("Trousers", 500);
      hm.put("Jeans", 1000);
      hm.put("Android TV",100000);
      hm.put("Air Purifiers", 5000);
      hm.put("Food Processors", 4000);
      System.out.println("Map = "+hm);
      Map<String,Integer> sort = new TreeMap<String, Integer>(hm);
      System.out.println("Sorted Map based on key = "+sort);
   }
}