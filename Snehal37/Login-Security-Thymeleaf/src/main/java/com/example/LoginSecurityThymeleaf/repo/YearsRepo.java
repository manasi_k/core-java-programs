package com.example.LoginSecurityThymeleaf.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.LoginSecurityThymeleaf.model.Years;

public interface YearsRepo extends JpaRepository<Years, Long> {

}
