package com.example.LoginSecurityThymeleaf.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.LoginSecurityThymeleaf.model.RewardType;

public interface RewardTypeRepo extends JpaRepository<RewardType, Long> {
	

}
