package com.example.LoginSecurityThymeleaf.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.LoginSecurityThymeleaf.service.UserService;
import com.example.LoginSecurityThymeleaf.web.dto.UserRegDto;



@Controller
@RequestMapping("/registration")
public class UserReg {

    private UserService userService;

    public UserReg(UserService userService) {
        super();
        this.userService = userService;
    }

    @ModelAttribute("user")
    public UserRegDto userRegistrationDto() {
        return new UserRegDto();
    }

    @GetMapping
    public String showRegistrationForm() {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") UserRegDto registrationDto) {
        userService.save(registrationDto);
        return "redirect:/registration?success";
    }
}
