package com.example.LoginSecurityThymeleaf.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity

public class RewardType {

	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "R_Id")
	private Long R_Id;
	private String Reward_Type;
	private String Status; 
	private String Description;

	@OneToMany(mappedBy = "rewardType", cascade = { CascadeType.ALL })

	private List<Rewards> rewards;

}
