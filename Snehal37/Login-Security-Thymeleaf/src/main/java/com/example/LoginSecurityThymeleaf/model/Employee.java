package com.example.LoginSecurityThymeleaf.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.jpa.repository.query.JpaCountQueryCreator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employee")
@SQLDelete(sql =
"UPDATE employee " +
        "SET deleted = true " +
        "WHERE emp_Id = ?",check=ResultCheckStyle.COUNT)

@Where(clause = "deleted = false")

public class Employee {

	@Id
	//@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long emp_Id;

	private String emp_Name;
	
	private String Status;
	@Lob
	public String image;
	
	 private boolean deleted;

	 	@ManyToOne
		@JoinColumn(name = "R_Id")
		private RewardType rewardType;
	
	@OneToMany(mappedBy = "employee", cascade = { CascadeType.ALL })
	private List<Rewards> rewards;
	
	@ManyToOne

	@JoinColumn(name = "dept_Id")
	private Department department;
	
	

//	@Override
//	public String toString() {
//		return "Employee [emp_Id=" + emp_Id + ", emp_Name=" + emp_Name + ", image=" + image + ", deleted=" + deleted
//				+ ", rewardType=" + rewardType + ", rewards=" + rewards + ", department=" + department + "]";
//	}
}