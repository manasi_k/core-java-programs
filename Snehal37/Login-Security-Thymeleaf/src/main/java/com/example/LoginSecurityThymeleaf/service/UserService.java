package com.example.LoginSecurityThymeleaf.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.multipart.MultipartFile;

import com.example.LoginSecurityThymeleaf.model.Department;
import com.example.LoginSecurityThymeleaf.model.User;
import com.example.LoginSecurityThymeleaf.web.dto.UserRegDto;

public interface UserService extends UserDetailsService{
	 User save(UserRegDto registrationDto);

	void saveEmployeeToDB(Long emp_Id, String emp_Name,Long dept_Id);
	}