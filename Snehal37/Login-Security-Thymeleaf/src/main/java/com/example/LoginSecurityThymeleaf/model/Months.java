package com.example.LoginSecurityThymeleaf.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Months {
	
	@Id
	private Long m_id;
	private String Month;
	
	@OneToMany(mappedBy = "months", cascade = { CascadeType.ALL })
	private List<Rewards> rewards;
	

}
