package com.example.LoginSecurityThymeleaf.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@SQLDelete(sql =
"UPDATE rewards " +
        "SET deleted = true " +
        "WHERE Id = ?",check=ResultCheckStyle.COUNT)

 

@Where(clause = "deleted = false")

public class Rewards {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	
	private boolean deleted;
	// private String R_Id;
	// private String status;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "R_Id", nullable = false)
	private RewardType rewardType;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "emp_Id", nullable = false)
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "m_id", nullable = false)
	private Months months;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "y_id", nullable = false)
	private Years years;

	

	

	 
	
	

}
