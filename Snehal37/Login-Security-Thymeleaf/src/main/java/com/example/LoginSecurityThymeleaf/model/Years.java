package com.example.LoginSecurityThymeleaf.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Years {

	@Id
	private Long y_id;
	private String Year;
	
	@OneToMany(mappedBy = "years", cascade = { CascadeType.ALL })
	private List<Rewards> rewards;
	
}
