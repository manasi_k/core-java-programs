package com.example.LoginSecurityThymeleaf.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.LoginSecurityThymeleaf.model.Months;

public interface MonthsRepo extends JpaRepository<Months , Long>{

}
