package com.examaple.Employee;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
 
public class EmployeeMain {
    public static void main(String[] args) {
    	 Employee e1 = new Employee(1, "Kalpesh", 30);
        Employee e2 = new Employee(2, "Snehal", 22);
        Employee e3 = new Employee(3, "Shraddha", 21);
        Employee e4 = new Employee(4, "Vishal", 24);
        Employee e5= new Employee(5, "Manasi", 22);
        Employee e6 = new Employee(6, "Ashwini", 22);
 
        List<Employee> employees = new ArrayList<Employee>();
        employees.add(e2);
        employees.add(e5);
        employees.add(e3);
        employees.add(e1);
        employees.add(e6);
        employees.add(e4);
       
       
 
        // UnSorted List
        System.out.println("Unsorted ::\n"+employees);
 
        Collections.sort(employees);
 
        // Default Sorting by employee id
        System.out.println("Sort by id::\n"+employees);
        
        Collections.sort(employees, new NameSort());
      //sort by Name
        System.out.println("Sort by Name::\n"+employees);
   
        Collections.sort(employees, new AgeSort());
        //sort by Age
          System.out.println("Sort by Age::\n"+employees);
          
     
    }
}