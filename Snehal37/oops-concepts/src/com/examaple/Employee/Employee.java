package com.examaple.Employee;

public class Employee implements Comparable<Employee> {
	 
    private int id = -1;
    private String Name = null;
    
    private int age = -1;
 
    public Employee(int id, String Name, int age) {
        this.id = id;
        this.Name = Name;
      
        this.age = age;
    }
    public String getName() {
        return Name;
    }
    public int getage() {
        return age;
    }
   
    @Override
    public int compareTo(Employee o) {
        return this.id - o.id;
       
        
    }
 
    // Setters and Getters
 
    @Override
    public String toString() {
        return "Employee : " + id + " - " + Name + " - " + age + "\n";
    }
}