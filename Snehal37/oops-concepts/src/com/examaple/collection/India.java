package com.examaple.collection;

import java.util.*;  
public class India {  
    public static void main(String[] args) {   
            List<String> arrlist = new ArrayList<String>();  
              
            arrlist.add("I");  
            arrlist.add("N");  
              arrlist.add("D");  
              arrlist.add("I");  
            arrlist.add("A");  
            System.out.println("List of elements: "+arrlist);  
                 
            System.out.println("I:"+Collections.frequency(arrlist,"I")); 

            System.out.println("N:"+Collections.frequency(arrlist,"N"));

            System.out.println("D:"+Collections.frequency(arrlist,"D"));

            System.out.println("A:"+Collections.frequency(arrlist,"A")); 
            }      
}  
