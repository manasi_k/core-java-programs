package com.examaple.collection;

public class StringBuff {
	   public static void main(String []args) {
	      stringBufferTest();
	      stringBuilderTest();
	   }
	   public static void stringBufferTest() {
	      long startTime = System.nanoTime();
	      StringBuffer sb = new StringBuffer();
	      for (int i=0; i < 1000; i++) {
	         sb.append((char) 'a');
	      }
	      System.out.println("StringBuffer test: " + (System.nanoTime() - startTime));
	   }
	   public static void stringBuilderTest() {
	      long startTime = System.nanoTime();
	      StringBuilder sb = new StringBuilder();
	      for (int i=0; i < 1000; i++) {
	         sb.append((char) 'a');
	      }
	      System.out.println("StringBuilder test: " + (System.nanoTime() - startTime));
	   }
	}