package com.example.java8;

 
import java.lang.reflect.Method;
import java.util.Arrays;
import java.lang.reflect.Field; 
import java.lang.reflect.Constructor; 


class Test 
{ 
	 
	private String s; 

	
	public Test() { s = "Neutrino Tech Systems"; } 

	public void fun() { 
		System.out.println("The string is: " + s); 
	} 

	
	public void gun(int n) { 
		System.out.println("The number is " + n); 
	} 

	
	private void sun() { 
		System.out.println("Private method invoked"); 
	} 
} 

class Reflection
{ 
	public static void main(String args[]) throws Exception 
	{ 
		
		Test obj = new Test(); 

		
		Class<? extends Test> cls = obj.getClass(); 
		System.out.println("The name of class is " + 
							cls.getName()); 

		
		Constructor<? extends Test> constructor = cls.getConstructor(); 
		System.out.	println("The name of constructor is " + 
							constructor.getName()); 

		System.out.println(" Methods of class are : "); 

		
		Method[] methods = cls.getDeclaredMethods(); 
		System.out.println("Declared Methods\n"+Arrays.toString(methods));

		
		
} 
}