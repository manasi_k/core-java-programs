package com.example.java8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerialDemo {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		Save obj = new Save();
		obj.i=4;
		
		File f =new File("demo.txt");
		FileOutputStream outs=new FileOutputStream(f); 
		ObjectOutputStream oos =new ObjectOutputStream(outs);
		oos.writeObject(obj);
		
		
		FileInputStream fis=new FileInputStream(f);
		ObjectInputStream ois=new ObjectInputStream(fis);
		Save obj1=(Save)ois.readObject();
		System.out.println("Value of obj1::" + obj1.i);
	}

}		
class Save implements Serializable{
	int i;
}