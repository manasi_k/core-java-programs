package com.example.StudentData.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.StudentData.Entity.Student;

public interface StudentSer extends CrudRepository<Student, Integer>{

}
