package com.example.StudentData.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.StudentData.Entity.Student;
import com.example.StudentData.dao.StudentSer;

@RestController
@RequestMapping("/student")
public class StudentConroller {
	@Autowired
	private StudentSer service;
	
	@PostMapping("/addstudent")
	public String addStudent(@RequestBody List<Student> student) {
		service.saveAll(student);
		return "record added"+student.size();
	}
	@GetMapping("/getstudent")
	public List<Student> getStudent(){
		return (List<Student>) service.findAll();
	}
}
