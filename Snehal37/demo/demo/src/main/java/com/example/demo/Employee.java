package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component //creates object in spring container 
public class Employee {
private int id;
private String name;
@Autowired //search by type
@Qualifier("lp1")//search by name
private Laptop laptop;

public Employee() {
	super();
	System.out.println("Object created");
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

public Laptop getLaptop() {
	return laptop;
}

public void setLaptop(Laptop laptop) {
	this.laptop = laptop;
}
public void show() {
	System.out.println("In Employee.show");
	laptop.ready();
}
}
