package com.example.demo;

import org.springframework.stereotype.Component;

@Component("lp1")
public class Laptop {
	private int id;
	private String brand;
	private String Ram;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getRam() {
		return Ram;
	}
	public void setRam(String ram) {
		Ram = ram;
	}
	@Override
	public String toString() {
		return "Laptop [id=" + id + ", brand=" + brand + ", Ram=" + Ram + "]";
	}
	public void ready() {
		System.out.println("Ready To Go");
	}
}
