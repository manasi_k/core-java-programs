package oop;

 class Demo1 {
	
	public void Display()
    {
        int a=10;
        int b=30;
        System.out.println(+a);
        System.out.println(+b);
    }
}

 public class MOverriding extends Demo1
{
    public void Display2()      {
       int a=20;
    	
        int c=50;
        System.out.println(+a);					//a is overrinding.
        System.out.println(+c);
    }
    
    public static void main(String[] args) {
    	MOverriding d = new MOverriding();
    	d.Display();
        d.Display2();
    }

}
