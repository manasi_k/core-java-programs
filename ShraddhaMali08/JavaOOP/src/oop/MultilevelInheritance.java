package oop;



class Base1{
int a = 10;
void Display() {
    System.out.println("a = "+a);
    System.out.println("Base1");
}
}

class Base2 extends Base1{
int b = 20;
void Display2() {
    System.out.println("b = "+b);
    System.out.println("Base2");
}
}

public class MultilevelInheritance extends Base2{

public static void main(String[] args) {
MultilevelInheritance c = new MultilevelInheritance();

System.out.println("main method");
c.Display();
c.Display2();  
}
}