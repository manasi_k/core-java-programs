package oop;

public class MOverloading {
	 void sum (int a, int b)
	  {
	    System.out.println("sum is"+(a+b)) ;
	  }
	  void sum (float a, float b)
	  {
	    System.out.println("sum is"+(a+b));
	  }
	  public static void main (String[] args)
	  {
		  MOverloading  cal = new MOverloading();
	    cal.sum (10,5);      //sum(int a, int b) is method is called.
	    cal.sum (5.7f, 7.8f); //sum(float a, float b) is called.
	  }

}
