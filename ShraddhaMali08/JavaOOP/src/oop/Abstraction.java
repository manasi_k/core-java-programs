package oop;

abstract class CAbstract
{
  abstract void callme();
  
}
class Abstraction extends CAbstract
{
  void callme()
  {
    System.out.println("Calling...");
  }
  public static void main(String[] args)
  {  
	  Abstraction obj = new Abstraction();
    obj.callme();
  }
}