package reflection;

import java.lang.Class;
import java.lang.reflect.*;

class Bike {

	// public constructor without parameter
	public Bike() {

	}

	// private constructor with a single parameter
	private Bike(int CC) {

	}

}

public class Reflection_Constructor {
	public static void main(String[] args) {
		try {
			// create an object of Dog
			Bike d1 = new Bike();

			// create an object of Class
			// using getClass()
			Class obj = d1.getClass();

			// get all constructors of Dog
			Constructor[] constructors = obj.getDeclaredConstructors();

			for (Constructor c : constructors) {

				// get the name of constructors
				System.out.println("Constructor Name: " + c.getName());

				// get the access modifier of constructors
				// convert it into string form
				int modifier = c.getModifiers();
				String mod = Modifier.toString(modifier);
				System.out.println("Modifier: " + mod);

				// get the number of parameters in constructors
				System.out.println("Parameters: " + c.getParameterCount());
				System.out.println("");
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
