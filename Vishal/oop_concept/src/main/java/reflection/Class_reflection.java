package reflection;

import java.lang.Class;
import java.lang.reflect.*;

class Car {
}


public class Class_reflection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		try {
		      // create an object of Dog
		      BMW d1 = new BMW();

		     
		      Class obj = d1.getClass();

		     
		      String name = obj.getName();
		      System.out.println("Name: " + name);

		      int modifier = obj.getModifiers();

		
		      String mod = Modifier.toString(modifier);
		      System.out.println("Modifier: " + mod);

		
		      Class superClass = obj.getSuperclass();
		      System.out.println("Superclass: " + superClass.getName());
		    }

		    catch (Exception e) {
		      e.printStackTrace();
		    }
		  }
	}


