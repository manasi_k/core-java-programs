package reflection;

import java.lang.Class;
import java.lang.reflect.*;

class skoda {
	public String Engine;
}

public class Reflection_feild {

	public static void main(String[] args) {
		try {

			skoda d1 = new skoda();

			Class obj = d1.getClass();

			Field field1 = obj.getField("Engine");
			field1.set(d1, "24HP");

			String typeValue = (String) field1.get(d1);
			System.out.println("Value: " + typeValue);

			int mod = field1.getModifiers();

			String modifier1 = Modifier.toString(mod);
			System.out.println("Modifier: " + modifier1);
			System.out.println(" ");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}