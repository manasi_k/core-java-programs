

public class Encapsulation_Abstraction 
{ 
    //only access by this class
    private String Name; 
    private int Roll; 
    private int Age; 
  
    
    public int getAge()  
    { 
      return Age; 
    } 
   
    public String getName()  
    { 
      return Name; 
    } 
      
    public int getRoll()  
    { 
       return Roll; 
    } 
   
    public void setAge( int newAge) 
    { 
      Age = newAge; 
    } 
   
    public void setName(String newName) 
    { 
      Name = newName; 
    } 
      
    public void setRoll( int newRoll)  
    { 
      Roll = newRoll; 
    } 


	public static void main (String[] args)  
    { 
		Encapsulation_Abstraction obj = new Encapsulation_Abstraction(); 
          
        // setting values of the variables  
        obj.setName("Vishal"); 
        obj.setAge(23); 
        obj.setRoll(90); 
          
        // Displaying values of the variables 
        System.out.println("name: " + obj.getName()); 
        System.out.println("Gage: " + obj.getAge()); 
        System.out.println("roll: " + obj.getRoll()); 

}
}
