package corejava.interface_inheritance;


	
	interface one 
	{ 
	    public void print_one(); 
	} 
	   
	interface two 
	{ 
	    public void print_two(); 
	} 
	   
	interface three extends one,two 
	{ 
	    public void print_three(); 
	} 
	
	class child implements three 
	{ 
	    @Override
	    public void print_one() { 
	        System.out.println("Interface one implimented"); 
	    } 
	   
	    public void print_two() 
	    { 
	        System.out.println("interface two implimented"); 
	    } 
	    
	    public void print_three()
	    { 
	        System.out.println("interface three implimented"); 
	    } 
	} 
	
	public class Multiple_Inheritance {
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		child c = new child(); 
        c.print_one(); 
        c.print_two(); 
        c.print_three(); 
	}

}
