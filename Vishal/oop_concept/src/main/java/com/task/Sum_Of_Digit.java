package com.task;
import java.util.*;

public class Sum_Of_Digit {
	
	public static void AddDigit(Integer i)
	{
		int digit;
		int sum=0;
		
		while(i>0)
		{
			digit=i%10;
			sum=sum+digit;
			i=i/10;
		}
		
		System.out.println("the sum of digit is ="+String.valueOf(sum));
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		while(true)
		{
			Scanner scan=new Scanner(System.in);
			System.out.println("enter the number or enter 0 to exit");
			int i=scan.nextInt();
			if(i==0)
			{
				break;
			}
			System.out.println("you enter : "+ Integer.toString(i));
			AddDigit(i);
			
		}
		System.out.println("exit");

	}

}
