package com.task;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class StreamInJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		while (true)
		{
			Scanner scan = new Scanner(System.in);
			System.out.println("enter the number or enter 0 to exit");
			String i = scan.nextLine();
			if (i.equals("0")) 
			{
				break;
			}
			System.out.println("you enter : " + i);

			int size = i.length();
			int[] arr = new int[size];
			for (int j=0; j < size; j++) 
			{
				arr[j] = Character.getNumericValue(i.charAt(j)); 
			}

			////////////// stream to add number of arrays

			int sum = Arrays.stream(arr).sum();
			System.out.println("the sum of number is " + Integer.toString(sum));

		}
		System.out.println("exit");
	}

}
