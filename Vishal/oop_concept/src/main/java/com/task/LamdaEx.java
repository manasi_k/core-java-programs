package com.task;

import java.util.Arrays;
import java.util.Scanner;

public class LamdaEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		while (true)
		{
			Scanner scan = new Scanner(System.in);
			System.out.println("enter the number or enter 0 to exit");
			String i = scan.nextLine();
			if (i.equals("0")) 
			{
				break;
			}
			System.out.println("you enter : " + i);

			int size = i.length();
			int[] arr = new int[size];
			for (int j=0; j < size; j++) 
			{
				arr[j] = Character.getNumericValue(i.charAt(j)); 
			}

			////////////// lamda function to find even element

			Arrays.stream(arr).forEach(n -> { if (n%2 == 0) System.out.println(n); });

		}
		System.out.println("exit");
	}
	}


