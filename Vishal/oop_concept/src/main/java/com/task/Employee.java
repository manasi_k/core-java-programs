package com.task;

import java.util.Comparator;

public class Employee {
	private String ID, Name;
	private Integer Age;
	
	public Employee(String id,String name,Integer age)
	{
		this.ID=id;
		this.Name=name;
		this.Age=age;	
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getName() {	
		return Name;
	}

	@Override
	public String toString() {
		return "Employee [ID=" + ID + ", Name=" + Name + ", Age=" + Age + "]";
	}

	public void setName(String name) {
		Name = name;
	}

	public Integer getAge() {
		return Age;
	}

	public void setAge(Integer age) {
		Age = age;
	}
	
	
	
	public static Comparator<Employee> EmpIDComp = new Comparator<Employee>() {

		public int compare(Employee s1, Employee s2) {
		   String emp1 = s1.getID().toUpperCase();
		   String emp2 = s2.getID().toUpperCase();

		   //ascending order
		   return emp1.compareTo(emp2);

		   //descending order
		   //return StudentName2.compareTo(StudentName1);
	    }};
	
	 public static Comparator<Employee> EmpNameComp = new Comparator<Employee>() {

			public int compare(Employee s1, Employee s2) {
			   String emp1 = s1.getName().toUpperCase();
			   String emp2 = s2.getName().toUpperCase();

			   //ascending order
			   return emp1.compareTo(emp2);

			   //descending order
			   //return StudentName2.compareTo(StudentName1);
		    }};

		    /*Comparator for sorting the list by roll no*/
		    public static Comparator<Employee> EmpAgeComp = new Comparator<Employee>() {

			public int compare(Employee s1, Employee s2) {

			   int emp1 = s1.getAge();
			   int emp2 = s2.getAge();

			   /*For ascending order*/
			   return emp1-emp2;

			   /*For descending order*/
			   //rollno2-rollno1;
		   }};

};