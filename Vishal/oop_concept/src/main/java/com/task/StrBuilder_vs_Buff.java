package com.task;

public class StrBuilder_vs_Buff {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringBuilder s2 = new StringBuilder("welcome"); 
        System.out.println("StringBuilder: " + s2); 
        s2.append("NTS");
        System.out.println(s2);
  
        StringBuffer s3 = new StringBuffer("welcome"); 
        System.out.println("StringBuffer: " + s3);
        s3.append("INDIA");
        System.out.println(s3);
	}

}
