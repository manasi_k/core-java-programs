package com.task;
import java.util.*;

public class Frequency_of_word {
	
	public static void CharacterCount(String str)
	{
		HashMap<Character, Integer> Cmap= new HashMap<Character, Integer>();
		
		char[] StrArray=str.toCharArray();
		
		for(char c: StrArray)
		{
			if(Cmap.containsKey(c))
			{
				Cmap.put(c, Cmap.get(c) + 1);
			}
			else
			{
				Cmap.put(c, 1);
			}
		}
		
		for(Character key: Cmap.keySet())
		{
			System.out.println(key + " : " + Cmap.get(key));
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while(true)
		{
		Scanner ip=new Scanner(System.in);
		System.out.println("enter the String or enter 0 to exit");
		String str=ip.nextLine();
		if(str.equals("0"))
		{
			break;
		}
		System.out.println("The given string is = " +str);
		CharacterCount(str);
		}

	}

}
