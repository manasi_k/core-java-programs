package com.task;

import java.util.*;

//employee object (ID, Name, Age) - sort list of employee based on ID, Name & Age

public class Employee_Portal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee emp1 = new Employee("N90", "Vishal", 23);
		Employee emp2 = new Employee("N93", "Snehal", 25);
		Employee emp3 = new Employee("N80", "shradha", 21);
		Employee emp5 = new Employee("N95", "Mansi", 24);

		ArrayList<Employee> EL = new ArrayList<Employee>();
		EL.add(emp1);
		EL.add(emp2);
		EL.add(emp3);
		EL.add(emp5);

		System.out.println("before sort");

		for (Employee str : EL) {
			System.out.println(str);
		}

		Collections.sort(EL, Employee.EmpAgeComp);

		System.out.println("After Age sort");

		for (Employee str : EL) {
			System.out.println(str);
		}

		Collections.sort(EL, Employee.EmpNameComp);

		System.out.println("After Name sort");

		for (Employee str : EL) {
			System.out.println(str);
		}

		Collections.sort(EL, Employee.EmpIDComp);

		System.out.println("After ID sort");

		for (Employee str : EL) {
			System.out.println(str);
		}

	}

}
