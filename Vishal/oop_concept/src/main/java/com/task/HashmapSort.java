package com.task;

import java.util.*;
import java.util.HashMap;
import java.util.Scanner;

public class HashmapSort {

	public static HashMap<Character, Integer> CharacterCount(String str) {
		HashMap<Character, Integer> Cmap = new HashMap<Character, Integer>();

		char[] StrArray = str.toCharArray();

		for (char c : StrArray) {
			if (Cmap.containsKey(c)) {
				Cmap.put(c, Cmap.get(c) + 1);
			} else {
				Cmap.put(c, 1);
			}
		}

		for (Character key : Cmap.keySet()) {
			System.out.println(key + " : " + Cmap.get(key));
		}

		return Cmap;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		while (true) {
			Scanner ip = new Scanner(System.in);
			System.out.println("enter the String or enter 0 to exit");
			String str = ip.nextLine();
			if (str.equals("0")) {
				break;
			}
			System.out.println("The given string is = " + str);
			HashMap<Character, Integer> HM = CharacterCount(str);
			
			////Sort by key
			
			Map<Integer, String> mapkey = sortBykey(HM);
			System.out.println("After Sorting by Keys:");
			Set set = mapkey.entrySet();
			Iterator iterator1 = set.iterator();
			while (iterator1.hasNext()) {
				Map.Entry me2 = (Map.Entry) iterator1.next();
				System.out.print(me2.getKey() + ": ");
				System.out.println(me2.getValue());
			}

			Map<Integer, String> map = sortByValues(HM);
			System.out.println("After Sorting by value:");
			Set set2 = map.entrySet();
			Iterator iterator2 = set2.iterator();
			while (iterator2.hasNext()) {
				Map.Entry me2 = (Map.Entry) iterator2.next();
				System.out.print(me2.getKey() + ": ");
				System.out.println(me2.getValue());
			}
		}
	}

	private static HashMap sortByValues(HashMap map) {
		List list = new LinkedList(map.entrySet());
		// Defined Custom Comparator here
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
			}
		});
		// Here I am copying the sorted list in HashMap
		// using LinkedHashMap to preserve the insertion order
		HashMap sortedHashMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedHashMap.put(entry.getKey(), entry.getValue());
		}
		return sortedHashMap;
	}

	
	private static HashMap sortBykey(HashMap map) {
		List list = new LinkedList(map.entrySet());
		// Defined Custom Comparator here
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getKey()).compareTo(((Map.Entry) (o2)).getKey());
			}
		});
		// Here I am copying the sorted list in HashMap
		// using LinkedHashMap to preserve the insertion order
		HashMap sortedHashMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedHashMap.put(entry.getKey(), entry.getValue());
		}
		return sortedHashMap;
	}
}
