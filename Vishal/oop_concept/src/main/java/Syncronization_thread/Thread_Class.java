package Syncronization_thread;

class Hello extends Thread
{
	public void run()
	{
		System.out.println("Inside the Thread by extending thread class");
		for(int i=0;i<10;i++)
		{
			System.out.println(i);
		}
	}
}

class Hello1 implements Runnable
{
	public void run()
	{
		System.out.println("Inside the Thread by implimenting runnable interface");
		for(int i=0;i<10;i++)
		{
			System.out.println(i);
		}
	}
	
}

public class Thread_Class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("inside the Main Thread");
		Hello obj1=new Hello();
		Hello1 obj2=new Hello1();
		Thread T1=new Thread(obj1);
		Thread T2=new Thread(obj2);
		
		T1.start();
		T2.start();
	}

};
