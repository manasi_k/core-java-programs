package Syncronization_thread;

class SYNC extends Thread
{
	synchronized void fun()
	{
		System.out.println("Inside the Thread by extending thread class");
		for(int i=0;i<10;i++)
		{
			System.out.println(i);
			try
			{
				Thread.sleep(300);
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("inside catch");
			}
		}
	}
	public void run()
	{
		System.out.println(Thread.currentThread().getName());
		fun();
	}
}

public class Syncronized_ex {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("inside the Main Thread");
		SYNC obj1=new SYNC();
		SYNC obj2=new SYNC();
		
		Thread T1=new Thread(obj1,"First");
		Thread T2=new Thread(obj2,"Second");
		
		
		T1.start();
		T1.join();
		T2.start();
		T2.join();
		
	}

}
