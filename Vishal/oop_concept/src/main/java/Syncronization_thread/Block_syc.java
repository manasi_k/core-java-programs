package Syncronization_thread;

class Mult {

	void printTable(int n) {
		synchronized (this) {
			for (int i = 1; i <= 5; i++) {
				System.out.println(n * i);
				try {
					Thread.sleep(400);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
	}
}

class MyThread1 extends Thread {
	Mult t;

	MyThread1(Mult t) {
		this.t = t;
	}

	public void run() {
		t.printTable(5);
	}

}

class MyThread2 extends Thread {
	Mult t;

	MyThread2(Mult t) {
		this.t = t;
	}

	public void run() {
		t.printTable(100);
	}
}

public class Block_syc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Mult obj = new Mult();// only one object
		MyThread1 t1 = new MyThread1(obj);
		MyThread2 t2 = new MyThread2(obj);
		t1.start();
		t2.start();
	}

}
