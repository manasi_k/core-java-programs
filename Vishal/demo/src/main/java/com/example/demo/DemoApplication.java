package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context=SpringApplication.run(DemoApplication.class, args);
		System.out.println("Welcome to Spring Boot");
		
		//Singletone object creation
//		Car audi=context.getBean(Car.class);
//		audi.Show();
		
		
		//Prototype object creation
		Laptop dell=context.getBean(Laptop.class);
		dell.Show();
//		
//		Laptop Asus=context.getBean(Laptop.class);
//		Asus.Show();
		
		
	}

}
