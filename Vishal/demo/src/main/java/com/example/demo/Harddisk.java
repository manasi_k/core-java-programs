package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class Harddisk {
	private int Size;
	private String type;
	public int getSize() {
		return Size;
	}
	public void setSize(int size) {
		Size = size;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "Harddisk [Size=" + Size + ", type=" + type + "]";
	}

	void Show()
	{
		System.out.println("harddisk component accesed");
	}
}
